<?php

namespace App\Controller;

use App\Entity\Room;
use App\Form\RoomType;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\RoomRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * @Route("/room")
 */
class RoomController extends AbstractController
{

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }


    /**
     * @Route("/", name="room")
     */
    public function index(RoomRepository $RoomRepo): Response
    {

        $rooms = $RoomRepo->findAvailableRoom();

        return $this->render('room/index.html.twig', [
            'controller_name' => 'RoomController',
            'rooms'=>$rooms
        ]);
    }


    /**
     * @Route("/new", name="room.add", methods={"GET","POST"})
     * @return Response
     */
    public function new(Request $request) : Response {
        $room = new Room();
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);
        $room->setUser($this->security->getUser());

        if ($form->isSubmitted() && $form->isValid()){
            $room->setCreatedAt(new DateTimeImmutable());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($room);
            $entityManager->flush();
            $this->addFlash('success','La salle a bien été ajoutée');
            return $this->redirectToRoute('room');
        }

        return $this->render("room/new.html.twig", [
            'room' => $room,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/edit",name="room.edit", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     */
    public function edit(Request $request, Room $room) : Response
    {
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $room->setUpdatedAt(new DateTimeImmutable());
            $entityManager = $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success','La salle a bien été modifée');
            return $this->redirectToRoute('room');
        }
        return $this->render('room/edit.html.twig',[
            'room'=>$room,
            'form'=>$form->createView()
        ]);
    }


    /**
     * @Route("/{id}",name="room.delete", methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function delete(Request $request, Room $room) : Response
    {
        if($this->isCsrfTokenValid('delete'.$room->getId(),$request->request->get('_token'))){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($room);
            $entityManager->flush();

            $this->addFlash('success','La salle a bien été supprimée');
            return $this->redirectToRoute('room');
        }
    }

}
